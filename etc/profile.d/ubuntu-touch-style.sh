# So QtQuick Controls 2 apps default to Suru style
export QT_QUICK_CONTROLS_STYLE=Suru

# For Kirigami
export QT_QUICK_CONTROLS_MOBILE=1
